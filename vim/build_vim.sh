#!/bin/bash
git clone https://github.com/vim/vim.git
cd vim
./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp \
            --enable-pythoninterp \
            --with-python-config-dir=/usr/lib/python2.7/config-x86_64-linux-gnu \
            --enable-python3interp \
            --with-python-config-dir=/usr/lib/python3.6/config-3.6m-x86_64-linux-gnu \
            --enable-rubyinterp \
            --enable-perlinterp \
            --enable-luainterp \
            --enable-cscope \
            --enable-netbeans_intg \
			--prefix=/usr/local \
			--with-x

make
sudo make install
cd ..
rm -rf vim
