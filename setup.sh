#!/bin/bash

if [[ ! -e ~/dotfiles ]]
then
	cd ~
	git clone https://framagit/kantum/dotfiles
fi

cd $HOME/dotfiles
git pull

echo "zshrc"
rm $HOME/.zshrc
ln -s `pwd`/zsh/.zshrc $HOME

echo "vimrc"
rm -rf $HOME/.vim*
ln -s `pwd`/vim/.vim $HOME
ln -s `pwd`/vim/.vimrc $HOME

echo "redshift.conf"
rm $HOME/.config/redshift.conf
ln -s `pwd`/redshift.conf $HOME/.config/

echo "Xdefaults"
rm $HOME/.Xdefaults
ln -s `pwd`/rxvt/.Xdefaults $HOME

echo "weechat"
rm -rf $HOME/.weechat
ln -s `pwd`/weechat/.weechat $HOME

echo "tmux.conf"
rm $HOME/.tmux.conf
ln -s `pwd`/tmux/.tmux.conf $HOME
